﻿
using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;

/*
 * Generic Exception - OSINT Telegram Bot
 * BitBucket Repository:
 * https://bitbucket.org/yosuamartinez95/osint_telegrambot
 */
namespace GE_OSINT_TELEGRAM_BOT
{
    public static class Program
    {
        private static TelegramBotClient _telegramBotClient;

        public static async Task Main()
        {
            _telegramBotClient = new TelegramBotClient(Configuration.BotToken);

            User me = await _telegramBotClient.GetMeAsync();
            Console.Title = $"{me.FirstName} [@{me.Username}]" ?? "Bot";

            using var cts = new CancellationTokenSource();

            // StartReceiving does not block the caller thread. Receiving is done on the ThreadPool.
            ReceiverOptions receiverOptions = new() { AllowedUpdates = { } };

            _telegramBotClient.StartReceiving(
                        updateHandler: Handlers.HandleUpdateAsync,
                        errorHandler: Handlers.HandleErrorAsync,
                        receiverOptions: receiverOptions,
                        cancellationToken: cts.Token);

            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();

            // Send cancellation request to stop bot
            cts.Cancel();
        }
    }
}

