﻿using GE_OSINT_TELEGRAM_BOT.Entities;
using System.Threading.Tasks;

namespace GE_OSINT_TELEGRAM_BOT.Services
{
    public interface ISocialMediaApiServices
    {
        Task<ProfileInfo> GetProfileInfoByUserNameAsync(string userName);
    }
}
