﻿using GE_OSINT_TELEGRAM_BOT.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GE_OSINT_TELEGRAM_BOT.Services
{
    public class TwitterApiServices : ISocialMediaApiServices
    {
        public async Task<ProfileInfo> GetProfileInfoByUserNameAsync(string userName)
        {
            var msg = $"BUSCAR USUARIO EN TWITTER: {userName}";
            Console.WriteLine(msg);


            var client = new RestClient("https://api.twitter.com");
            var request = new RestRequest($"/2/users/by/username/{userName}?user.fields=created_at,location,verified,description,public_metrics,profile_image_url", Method.Get);
            request.AddHeader("Authorization", $"Bearer {Configuration.TwitterToken}");
            RestResponse response = await client.ExecuteAsync(request);
            Console.WriteLine(response.Content);

            TwitterInfo twitterInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<TwitterInfo>(response.Content);

            return twitterInfo.MapToProfileInfo();
        }
    }

}
