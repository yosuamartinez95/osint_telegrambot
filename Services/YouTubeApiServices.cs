﻿using GE_OSINT_TELEGRAM_BOT.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GE_OSINT_TELEGRAM_BOT.Services
{
    public class YouTubeApiServices : ISocialMediaApiServices
    {
        public async Task<ProfileInfo> GetProfileInfoByUserNameAsync(string userName)
        {
            var msg = $"BUSCAR USUARIO EN YOUTUBE: {userName}";
            Console.WriteLine(msg);


            var client = new RestClient("https://api.creatoriq.com");
            var request = new RestRequest($"/api/accountInfo", Method.Get);
            request.AddQueryParameter("source", "user");
            request.AddQueryParameter("network", "youtube");
            request.AddQueryParameter("user", userName);
            request.AddHeader("X-SOCIALEDGE-ID", Configuration.YoutubeToken);
            RestResponse response = await client.ExecuteAsync(request);
            Console.WriteLine(response.Content);

            YouTubeInfo youTubeInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<YouTubeInfo>(response.Content);

            return youTubeInfo.MapToProfileInfo();
        }
    }
}
