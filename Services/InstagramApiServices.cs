﻿using GE_OSINT_TELEGRAM_BOT.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GE_OSINT_TELEGRAM_BOT.Services
{
    public class InstagramApiServices : ISocialMediaApiServices
    {
        public async Task<ProfileInfo> GetProfileInfoByUserNameAsync(string userName)
        {
            var msg = $"BUSCAR USUARIO EN INSTAGRAM: {userName}";
            Console.WriteLine(msg);


            var client = new RestClient("https://api.creatoriq.com/api");
            var request = new RestRequest($"/social/accountInfo/", Method.Get);
            request.AddQueryParameter("source", "user");
            request.AddQueryParameter("network", "instagram");
            request.AddQueryParameter("user", userName);
            request.AddHeader("X-SOCIALEDGE-ID", Configuration.InstragramToken);
            RestResponse response = await client.ExecuteAsync(request);
            Console.WriteLine(response.Content);

            InstagramInfo instagramInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<InstagramInfo>(response.Content);

            return instagramInfo.MapToProfileInfo();
        }
    }
}
