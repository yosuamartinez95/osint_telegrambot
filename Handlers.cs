using GE_OSINT_TELEGRAM_BOT.Entities;
using GE_OSINT_TELEGRAM_BOT.Services;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace GE_OSINT_TELEGRAM_BOT
{
    public class Handlers
    {
        private static readonly ISocialMediaApiServices _twitterApiServices = new TwitterApiServices();
        private static readonly ISocialMediaApiServices _instagramApiServices = new InstagramApiServices();
        private static readonly ISocialMediaApiServices _youTubeApiServices = new YouTubeApiServices();

        public static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }

        public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            var handler = update.Type switch
            {
                // UpdateType.Unknown:
                // UpdateType.ChannelPost:
                // UpdateType.EditedChannelPost:
                // UpdateType.ShippingQuery:
                // UpdateType.PreCheckoutQuery:
                // UpdateType.Poll:
                UpdateType.Message => BotOnMessageReceived(botClient, update.Message!),
                UpdateType.EditedMessage => BotOnMessageReceived(botClient, update.EditedMessage!),
                UpdateType.CallbackQuery => BotOnCallbackQueryReceived(botClient, update.CallbackQuery!),
                UpdateType.InlineQuery => BotOnInlineQueryReceived(botClient, update.InlineQuery!),
                UpdateType.ChosenInlineResult => BotOnChosenInlineResultReceived(botClient, update.ChosenInlineResult!),
                _ => UnknownUpdateHandlerAsync(botClient, update)
            };

            try
            {
                await handler;
            }
            catch (Exception exception)
            {
                await HandleErrorAsync(botClient, exception, cancellationToken);
            }
        }

        private static async Task BotOnMessageReceived(ITelegramBotClient botClient, Message message)
        {
            Console.WriteLine($"Receive message type: {message.Type}");

            if (message.Type != MessageType.Text)
                return;

            Task<Message> action;

            action = message.Text!.Split(' ')[0] switch
            {
                "/twitter" => SendText(botClient, message, _twitterApiServices),
                "/instagram" => SendText(botClient, message, _instagramApiServices),
                "/youtube" => SendText(botClient, message, _youTubeApiServices),
                //"/facebook" => SendText(botClient, message, null),
                //"/tiktok" => SendText(botClient, message, null),

                "/help" => Help(botClient, message),
                "/start" => Help(botClient, message),
                "/repository" => Repository(botClient, message),
                //"/all" => SendInlineKeyboard(botClient, message, null),
                _ => Usage(botClient, message)
            };

            Message sentMessage = await action;
            Console.WriteLine($"The message was sent with id: {sentMessage.MessageId}");

            // Send only a text msg
            static async Task<Message> SendText(ITelegramBotClient botClient, Message message, ISocialMediaApiServices socialMediaApiServices)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                if (message.Text!.Split(' ').Length < 2)
                {
                    return await Usage(botClient, message);
                }

                // Simulate longer running task
                await Task.Delay(500);

                return await botClient.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text: MapProfileInfoToMessage(await socialMediaApiServices.GetProfileInfoByUserNameAsync(message.Text!.Split(' ')[1])),
                    parseMode: ParseMode.MarkdownV2
                );
            }

            // Send inline keyboard
            // You can process responses in BotOnCallbackQueryReceived handler
            static async Task<Message> SendInlineKeyboard(ITelegramBotClient botClient, Message message, ISocialMediaApiServices socialMediaApiServices)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                // Simulate longer running task
                await Task.Delay(500);
                /*
                InlineKeyboardMarkup inlineKeyboard = new(
                    new[]
                    {
                    // first row
                    new []
                    {
                        InlineKeyboardButton.WithCallbackData("1.1", "11"),
                        InlineKeyboardButton.WithCallbackData("1.2", "12"),
                    },
                    // second row
                    new []
                    {
                        InlineKeyboardButton.WithCallbackData("2.1", "21"),
                        InlineKeyboardButton.WithCallbackData("2.2", "22"),
                    },
                    });

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Choose",
                                                            replyMarkup: inlineKeyboard);
                */
                return await botClient.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text: MapProfileInfoToMessage(await socialMediaApiServices.GetProfileInfoByUserNameAsync(message.Text!.Split(' ')[1]))
                );
            }

            static async Task<Message> SendReplyKeyboard(ITelegramBotClient botClient, Message message)
            {
                ReplyKeyboardMarkup replyKeyboardMarkup = new(
                    new[]
                    {
                        new KeyboardButton[] { "1.1", "1.2" },
                        new KeyboardButton[] { "2.1", "2.2" },
                    })
                {
                    ResizeKeyboard = true
                };

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Choose",
                                                            replyMarkup: replyKeyboardMarkup);
            }

            static async Task<Message> RemoveKeyboard(ITelegramBotClient botClient, Message message)
            {
                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Removing keyboard",
                                                            replyMarkup: new ReplyKeyboardRemove());
            }

            static async Task<Message> SendFile(ITelegramBotClient botClient, Message message)
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                const string filePath = @"Files/tux.png";
                using FileStream fileStream = new(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                var fileName = filePath.Split(Path.DirectorySeparatorChar).Last();

                return await botClient.SendPhotoAsync(chatId: message.Chat.Id,
                                                      photo: new InputOnlineFile(fileStream, fileName),
                                                      caption: "Nice Picture");
            }

            static async Task<Message> RequestContactAndLocation(ITelegramBotClient botClient, Message message)
            {
                ReplyKeyboardMarkup RequestReplyKeyboard = new(
                    new[]
                    {
                    KeyboardButton.WithRequestLocation("Location"),
                    KeyboardButton.WithRequestContact("Contact"),
                    });

                return await botClient.SendTextMessageAsync(chatId: message.Chat.Id,
                                                            text: "Who or Where are you?",
                                                            replyMarkup: RequestReplyKeyboard);
            }

            static async Task<Message> Usage(ITelegramBotClient botClient, Message message)
            {
                // Simulate longer running task
                await Task.Delay(500);

                return await botClient.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: "El comando introducidor no se ha podido procesar."
                             + "\n\nPara buscar informaci�n de un usuario debes emplear el formato: \n/comando usuario"
                             + "\n\nEjemplo:\n/twitter alo_oficial"
                             + "\n\nCon el comando /help puedes encontrar m�s informaci�n."
                    );
            }

            static async Task<Message> Help(ITelegramBotClient botClient, Message message)
            {
                // Simulate longer running task
                await Task.Delay(500);

                User me = await botClient.GetMeAsync();

                return await botClient.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: $"_*Informaci�n sobre {me.FirstName} [@{me.Username}]*_"
                             + "\n\nVersi�n: 0\\.1"
                             + "\n\nCon ese bot puedes buscar informaci�n de un perfil de una de las siguientes redes sociales:"
                             + "\n\n  \\> Twitter"
                             + "\n  \\> Instagram"
                             + "\n  \\> YouTube"
                             + "\n\nPara buscar informaci�n de un usuario debes emplear el formato: \n/comando usuario"
                             + "\n\nEjemplo:\n/twitter alo\\_oficial"
                             + "\n\n\nEl repositorio del proyecto es p�blico, puedes encontrarlo en /repository",
                        parseMode: ParseMode.MarkdownV2
                    );
            }

            static async Task<Message> Repository(ITelegramBotClient botClient, Message message)
            {
                // Simulate longer running task
                await Task.Delay(500);

                User me = await botClient.GetMeAsync();

                return await botClient.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: $"_*Repositorio del proyecto*_"
                             + "\n\nEl proyecto de GE\\-OSINT est� enfocado en el aprendizaje, por lo que es de c�digo abierto para que cualquiera pueda utilizarlo\\."
                             + "\nPuedes encontrar el repositorio en el siguiente enlace:"
                             + "\nhttps://bitbucket\\.org/yosuamartinez95/osint\\_telegrambot",
                        parseMode: ParseMode.MarkdownV2
                    );
            }
        }

        private static string MapProfileInfoToMessage(ProfileInfo profileInfo)
        {
            string text = $"_*Informaci�n de {profileInfo.SocialMediaName}*_\n";

            if (!String.IsNullOrEmpty(profileInfo.Id))
                text += $"\n*ID:* {profileInfo.Id}";

            if (!String.IsNullOrEmpty(profileInfo.UserName))
                text += $"\n*Usuario:* {profileInfo.UserName}";

            if (!String.IsNullOrEmpty(profileInfo.CompleteName))
                text += $"\n*Nombre:* {profileInfo.CompleteName}";

            if (!String.IsNullOrEmpty(profileInfo.Description))
                text += $"\n*Descripci�n:* {ConvertChars(profileInfo.Description)}";

            if (profileInfo.CreationDate.HasValue)
                text += $"\n*Fecha creaci�n:* {profileInfo.CreationDate}";

            text += $"\n*Cuenta privada:* {profileInfo.IsPrivate}"
                  + $"\n*Cuenta verificada:* {profileInfo.IsVerified}";

            if (profileInfo.Followers.HasValue)
                text += $"\n*Seguidores:* {profileInfo.Followers}";

            if (profileInfo.Posts.HasValue)
                text += $"\n*Publicaciones:* {profileInfo.Posts}";

            if (profileInfo.AverageMonthlyViews.HasValue)
                text += $"\n*Visualizaciones mensuales:* {profileInfo.AverageMonthlyViews}";

            if (!String.IsNullOrEmpty(profileInfo.ProfilePicture))
                text += $"\n*Imagen de perfil:* \\({ConvertChars(profileInfo.ProfilePicture)}\\)";

            return text;
        }

        // Process Inline Keyboard callback data
        private static async Task BotOnCallbackQueryReceived(ITelegramBotClient botClient, CallbackQuery callbackQuery)
        {
            await botClient.AnswerCallbackQueryAsync(
                callbackQueryId: callbackQuery.Id,
                text: $"Received {callbackQuery.Data}");

            await botClient.SendTextMessageAsync(
                chatId: callbackQuery.Message.Chat.Id,
                text: $"Received {callbackQuery.Data}");
        }

        private static async Task BotOnInlineQueryReceived(ITelegramBotClient botClient, InlineQuery inlineQuery)
        {
            Console.WriteLine($"Received inline query from: {inlineQuery.From.Id}");

            InlineQueryResult[] results = {
            // displayed result
            new InlineQueryResultArticle(
                id: "3",
                title: "TgBots",
                inputMessageContent: new InputTextMessageContent(
                    "hello"
                )
            )
        };

            await botClient.AnswerInlineQueryAsync(inlineQueryId: inlineQuery.Id,
                                                   results: results,
                                                   isPersonal: true,
                                                   cacheTime: 0);
        }

        private static Task BotOnChosenInlineResultReceived(ITelegramBotClient botClient, ChosenInlineResult chosenInlineResult)
        {
            Console.WriteLine($"Received inline result: {chosenInlineResult.ResultId}");
            return Task.CompletedTask;
        }

        private static Task UnknownUpdateHandlerAsync(ITelegramBotClient botClient, Update update)
        {
            Console.WriteLine($"Unknown update type: {update.Type}");
            return Task.CompletedTask;
        }

        private static string ConvertChars(string message)
        {
            return message
                .Replace(".", "\\.")
                .Replace("_", "\\_")
                .Replace("-", "\\-")
                .Replace("=", "\\=")
                .Replace("!", "\\!")
                .Replace("�", "\\�")
                .Replace("?", "\\?")
                .Replace("�", "\\�")
                .Replace("@", "\\@")
                .Replace("$", "\\$")
                .Replace("#", "\\#");
        }
    }

}

