﻿using System;

namespace GE_OSINT_TELEGRAM_BOT.Entities
{
    public class TwitterInfo
    {
        public TwitterInfoProfile Data { get; set; }

        public ProfileInfo MapToProfileInfo()
        {
            return new ProfileInfo
            {
                SocialMediaName = "Twitter",
                Id = Data.Id,
                UserName = Data.Username,
                CompleteName = Data.Name,
                Description = Data.Description,
                IsVerified = Data.Verified ?? false,
                CreationDate = DateTime.Parse(Data.Created_At),
                ProfilePicture = Data.Profile_Image_Url,
                Posts = Data.Public_Metrics.Tweet_Count,
                Followers = Data.Public_Metrics.Followers_Count
            };
        }
    }

    public class TwitterInfoProfile
    {
        public string Username { get; set; }
        public bool? Verified { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public PublicMetrics Public_Metrics { get; set; }
        public string Created_At { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Profile_Image_Url { get; set; }
    }

    public class PublicMetrics
    {
        public int? Followers_Count { get; set; }
        public int? Following_Count { get; set; }
        public int? Tweet_Count { get; set; }
        public int? Listed_Count { get; set; }
    }
}
