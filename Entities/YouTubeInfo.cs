﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GE_OSINT_TELEGRAM_BOT.Entities
{
    public class YouTubeInfo
    {
        public AccountInfo AccountInfo { get; set; }

        public ProfileInfo MapToProfileInfo()
        {
            return new ProfileInfo
            {
                SocialMediaName = "YouTube",
                Id = AccountInfo.Account.Channel.ChannelId,
                UserName = AccountInfo.Account.Channel.ChannelName,
                ProfilePicture = AccountInfo.Account.Channel.LogoHighResURL,
                Description = AccountInfo.Account.Channel.Summary,
                IsVerified = AccountInfo.Account.Channel.Verified ?? false,
                CreationDate = DateTime.Parse(AccountInfo.Account.Channel.CreateDateTime),
                Followers = AccountInfo.Account.Channel.TotalSubscribers,
                Posts = AccountInfo.Account.Channel.TotalVideos,
                AverageMonthlyViews = AccountInfo.Account.Channel.AverageMonthlyViews,
            };
        }
    }

    public class AccountInfo
    {
        public Account Account { get; set; }
    }

    public class Account
    {
        public Channel Channel { get; set; }
    }

    public class Channel
    {
        public string ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string LogoHighResURL { get; set; }
        public string Summary { get; set; }
        public int? TotalSubscribers { get; set; }
        public int? TotalVideos { get; set; }
        public string CreateDateTime { get; set; }
        public int? AverageMonthlyViews { get; set; }
        public bool? Verified { get; set; }
    }
}
