﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GE_OSINT_TELEGRAM_BOT.Entities
{
    public class ProfileInfo
    {
        public string SocialMediaName { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string CompleteName { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsVerified { get; set; }
        public string ProfilePicture { get; set; }
        public string Description { get; set; }
        public int? Followers { get; set; }
        public int? Posts { get; set; }
        public int? AverageMonthlyViews { get; set; }
    }
}
