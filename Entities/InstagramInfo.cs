﻿using System;

namespace GE_OSINT_TELEGRAM_BOT.Entities
{
    public class InstagramInfo
    {
        public InstagramInfoProfile InstagramAccount { get; set; }

        public ProfileInfo MapToProfileInfo()
        {
            return new ProfileInfo
            {
                SocialMediaName = "Instagram",
                Id = InstagramAccount.Id,
                UserName = InstagramAccount.Name,
                CompleteName = InstagramAccount.Name,
                Description = InstagramAccount.Bio,
                IsVerified = InstagramAccount.Verified ?? false,
                //CreationDate = DateTime.Parse(InstagramAccount.CreatedAt),
                ProfilePicture = InstagramAccount.ProfileImageUrl,
                Posts = InstagramAccount.Posts,
                Followers = InstagramAccount.Followers
            };
        }
    }

    public class InstagramInfoProfile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool? Verified { get; set; }
        public string CreatedAt { get; set; }
        public string Bio { get; set; }
        public string Url { get; set; }
        public int? Posts { get; set; }
        public int? Followers { get; set; }
        public string ProfileImageUrl { get; set; }

    }
}
