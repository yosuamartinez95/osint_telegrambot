# GE - OSINT Telegram Bot #

Este proyecto está enfocado en el aprendizaje.
Consiste en un bot de telegram que permite obtener información de los perfiles de distintas redes sociales de un usuario.


### Redes sociales disponibles ###

* Twitter
* Instagram
* YouTube
* Facebook (en proceso)
* TikTok (por hacer)
* SnapChat (por hacer)
* LinkedIn (por hacer)

### Documentación bot de Telegram ###

* Documentación de Telegram: https://core.telegram.org/bots
* BotFather: https://t.me/botfather
* Librería de bot de Telegram para .Net: https://github.com/TelegramBots/Telegram.Bot

### Documentación del API de las redes sociales ###

* Twitter: https://developer.twitter.com/en/docs/twitter-api
* Instagram: 
* Facebook: https://developers.facebook.com/docs/
* Snapchat: https://developers.snap.com/
* LinkedIn: https://docs.microsoft.com/en-us/linkedin/consumer/

------------------------------------------------------

### Versiones ###

## Versión v0.1 ##

Redes sociales disponibles: Twitter e Instagram.
Obtención de datos básica.

## Versión v0.2 ##

Nueva red social añadida: YouTube.
Mejorada la obtención de datos de las redes sociales.
